Tramway React is a simple React add-on to the Tramway framework to simplify the integration of React components while using a consistent architecture in your project. It adds:

1. A new `ReactController` which abstracts server-side rendering logic.
2. Access to the `React` and `Component` classes from `react` so you won't also have to install `react` and `react-dom`.

# Installation:
1. `npm install tramway-core-react`
2. Add the necessary React Babel presets to your builder.

# Example project
https://gitlab.com/tramwayjs/tramway-react-example

# Documentation

## Recommended Folder Structure in addition to Tramway
- components
- views

## ReactController
The big addition with this extension is the `ReactController` which abstracts the `Component` binding process for your express-based app.

When creating a controller, extend the `ReactController` and return its render function.

```
import {controllers} from 'tramway-core-react';
let {ReactController} = controllers;
```

Here is a sample implementation with a Component called App which displays a simple message.
```
import App from '../components/App';
import {controllers} from 'tramway-core-react';
let {ReactController} = controllers;

export default class MainController extends ReactController {
    static index(req, res) {
        return ReactController.render(res, App, {'message': `Page rendered`});
    }
}
```

## Components
Components are the heart of your react views. Every react view extends the react `Component` class which is given to you as is.

To create a component, import the class and implement a derived class using Facebook's react documentation.
```
import {Component, React} from 'tramway-core-react';
```

## Views
You will need to choose and install a simple templating engine and create a file named `index.{extensionofyourtemplateengine}`. This would be set up in your server.js file with Tramway.
In it, ensurew there is a variable called `app` that is configured to display the string as HTML.

If you want a simple template engine, you could use EJS. To do so:

1. Install and save it to the project: `npm install --save ejs`
2. In your server.js file, add the following code after you instantiate the express app:

```
app.use(express.static(__dirname + '/public'));
app.set('views', 'dist/views');
app.set('view engine', 'ejs');
```

3. Create a `index.ejs` file with the following (you can modify this to your liking) contents:

```
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <%- app %>
</body>
</html>
```

4. Make all `Controller`s extend `ReactController` and use its render function.