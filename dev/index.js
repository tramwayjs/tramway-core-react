import * as controllers from './core/controllers';
import React, {Component} from 'react';

export {controllers, Component, React};