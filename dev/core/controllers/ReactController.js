import {Controller} from 'tramway-core';
import React, {Component} from 'react';
import ReactDOMServer from 'react-dom/server';

/**
 * @export
 * @class ReactController
 * @extends {Controller}
 */
export default class ReactController extends Controller {

    /**
     * Creates an instance of ReactController.
     * @memberOf ReactController
     */
    constructor() {
        super();
    }

    /**
     * @static
     * @param {ServerResponse} res
     * @param {Component} App
     * @param {Object} props
     * @returns
     * 
     * @memberOf ReactController
     */
    static render(res, App, props) {
        return res.render('index', {app: ReactDOMServer.renderToStaticMarkup(<App {...props}/>)});
    }
}